var plugins = require('gulp-load-plugins')();
var gulp = require('gulp');
var folders = require('./settings.js');

gulp.task('sass', function () {
  gulp.src(folders.source + '/scss/importer.scss')
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({
            includePaths: ['./node_modules/']
        }).on('error', plugins.sass.logError)
    )
    .pipe(plugins.autoprefixer())
    .pipe(plugins.rename('index.css'))
    .pipe(plugins.sourcemaps.write('/'))
    .pipe(gulp.dest(folders.dist + '/css'));
});

gulp.task('css-minify', function () {
  gulp.src(folders.dist + '/css/index.css')
    .pipe(plugins.cssmin({keepSpecialComments: 0}))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(folders.dist + '/css'));
});

var plugins = require('gulp-load-plugins')();
var gulp = require('gulp');
var folders = require('./settings.js');
