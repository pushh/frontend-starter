var gulp = require('gulp');
var requireDir = require('require-dir');
var folders = require('./gulp-tasks/settings.js');

requireDir('./gulp-tasks');

gulp.task('watch', ['sass', 'js'], function() {
  gulp.watch(folders.source + '/scss/**/*.{scss,sass}', ['sass']);
  gulp.start('js');
});

gulp.task('build', ['sass', 'js', 'css-minify', 'js-clean'])
gulp.task('default', ['watch']);

